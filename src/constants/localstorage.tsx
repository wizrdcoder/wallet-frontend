export class LocalstorageConstants {
  public static readonly ADMIN_USERNAME_KEY = 'admin_username';
  public static readonly ADMIN_PASSWORD_KEY = 'admin_password';
  public static readonly USER_DATA_KEY = 'user_data';
  public static readonly AUTH_TOKEN_KEY = 'token';
  public static readonly LOGGED_IN_USER_KEY = 'loggedInUser';
}