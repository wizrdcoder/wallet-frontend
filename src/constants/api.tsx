export class APIConstants {
  public static readonly BASE_ROUTE = 'https://damp-refuge-14447.herokuapp.com/api/v1/';
  public static readonly ADMIN_LOGIN_ROUTE = '/login.php';
  public static readonly GET_PROJECTS_ROUTE = '/getprojects.php';
  public static readonly ADD_PROJECT_ROUTE = '/addproject.php';
  public static readonly DELETE_PROJECT_ROUTE = '/deleteproject.php';
  public static readonly EDIT_PROJECT_ROUTE = '/editproject.php';
  public static readonly CHANGE_STATUS_ROUTE = '/changestatus.php';

  public static readonly SIGNUP_ROUTE = 'auth/signup';
  public static readonly LOGIN_ROUTE = 'auth/login';
  public static readonly WALLET_BALANCE_ROUTE = 'auth/wallet';
  public static readonly ALL_USERS_ROUTE = 'auth/users';
  public static readonly ADD_MONEY_ROUTE = 'auth/wallet/add';
  public static readonly SEND_MONEY_ROUTE = 'auth/wallet/send';
  public static readonly HISTORY_ROUTE = 'auth/wallet/history';
}