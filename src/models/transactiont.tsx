import Transaction from '../interfaces/transaction';

export default class NewTransaction implements Transaction {
  id!: number;
  user_id!: number;
  amount!: string;
  from!: string;
  type!: string;
  remarks!: string;
  created_at!: string;
  updated_at!: string;

  constructor() {}
}