import User from '../interfaces/user';

export default class NewUser implements User {
  id!: number;
  first_name!: string;
  last_name!: string;
  email!: string;
  email_verified_at!: string;
  created_at!: string;
  updated_at!: string;

  constructor() {}
}