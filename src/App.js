import logo from './logo.svg';
import './App.css';
import {
  BrowserRouter as Router,
  Switch,
  Route
} from 'react-router-dom';
import { Provider } from 'react-redux';

import configReduxStore from './redux/store';

// import Home from './domains/User';
// import Admin from './domains/Admin';
import Home from './domains';
import Dashboard from './domains/Dashboard';
import Login from './domains/Login';
import Register from './domains/Register';

function App() {
  return (
    <Provider store={configReduxStore()}>
      <Router>
        {/* <Switch>
          <Route path="/">
            <Home />
          </Route>
        </Switch> */}
        <Switch>
          <Route path={`/login`} >
            <Login />
          </Route>
          <Route path={`/register`}>
            <Register />
          </Route>
          <Route path="/" >
            <Dashboard />
          </Route>
        </Switch>
      </Router>
    </Provider>
  );
}

export default App;
