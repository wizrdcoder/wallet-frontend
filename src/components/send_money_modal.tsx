import { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';

// import AddProjectProps from '../interfaces/add_project_props';
import SendMoneyProps from '../interfaces/send_money_props';
import { RootState } from '../redux/types';
import {
  sendMoney
} from '../redux/wallet/actions';

import Modal from 'react-bootstrap/Modal';
import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form';
import Alert from 'react-bootstrap/Alert';
import Spinner from 'react-bootstrap/Spinner';

let SendMoneyModal: any = function(props: SendMoneyProps) {
  // if (props.project === null) {
  //   return null;
  // }

  const dispatch = useDispatch();
  const [showFormError, setShowFormError] = useState(false);
  const [formData, setFormData] = useState<any>({
    amount: null,
    id: null
  });
  const [isLoading, setIsLoading] = useState(false);
  const [showError, setShowError] = useState(false);
  const [requestError, setRequestError] = useState<String>('');
  const [showSuccess, setShowSuccess] = useState(false);
  
  if ((props.users && props.users.length > 0) && !formData.id) {
    setFormData({
      ...formData,
      id: props.users[0].id
    });
  }
  
  return(
    <Modal show={props.show} onHide={() => props.onClose()} size="lg" scrollable centered>
      <Modal.Header closeButton>
        <h5 className="modal-title">Send Money</h5>
      </Modal.Header>
      <Modal.Body className="pb-4 mx-md-5">
        {
          showFormError
          ? <Alert variant="danger" onClose={() => setShowFormError(false)} dismissible>
              <p>
                All fields are required
              </p>
            </Alert>
          : null
        }
        {
          showError
          ? <Alert variant="danger" onClose={() => {}} dismissible>
              <Alert.Heading>Oh snap! You got an error!</Alert.Heading>
              <p>
                {requestError}
              </p>
            </Alert>
          : null
        }
        {
          showSuccess
            ? <Alert variant="success" onClose={() => setShowSuccess(false)} dismissible>
                <p>
                  Transfer was made successfully
                </p>
              </Alert>
            : null
        }
          <Form>
            <Form.Group controlId="formBasicEmail">
              <Form.Label>Amount</Form.Label>
              <Form.Control
                type="number" 
                placeholder="Enter amount to send"
                onChange={(value: any) => {
                  setFormData({
                    ...formData,
                    amount: value["nativeEvent"]["target"]["value"]
                  });
                }}
                required/>
            </Form.Group>

            <Form.Group controlId="exampleForm.ControlSelect1">
              <Form.Label>Select Recipient</Form.Label>
              <Form.Control 
                as="select" 
                placeholder="Getting users..."
                onChange={(value: any) => {
                  setFormData({
                    ...formData,
                    id: parseInt(value["nativeEvent"]["target"]["value"])
                  });
                }
              }>
                {
                  props.users.map((user, i) => {
                    return <option 
                      key={i}
                      value={user.id}
                    >
                      {`${user.first_name} ${user.last_name}`}
                    </option>
                  })
                }
              </Form.Control>
            </Form.Group>
          </Form>
        
      </Modal.Body>
      <Modal.Footer>
        <Button variant="outline-secondary" onClick={() => {
          setIsLoading(false);
          setRequestError('');
          setShowError(false);
          setShowSuccess(false);
          props.onClose();
        }}>
          Close
        </Button>
        <Button 
          variant="primary" 
          disabled={isLoading}
          onClick={() => {
            if (!formData.amount || !formData.id) {
              setShowFormError(true);
              return;
            }

            setIsLoading(true);
            dispatch(
              sendMoney(
                formData.amount, 
                formData.id,
                () => {
                  setIsLoading(false);
                  setShowSuccess(true);
                },
                (errorMsg: String) => {
                  setIsLoading(false);
                  setRequestError(errorMsg);
                  setShowError(true);
                  setShowSuccess(false);
                }
              )
            );
          }
        }>
          Transfer
          {
            isLoading
              ? <Spinner
                  as="span"
                  animation="border"
                  size="sm"
                  role="status"
                  aria-hidden="true"
                  className="ml-2"
                /> 
              : null
          }
        </Button>
      </Modal.Footer>
    </Modal>
  );
};

export default SendMoneyModal;