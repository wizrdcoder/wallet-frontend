import { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';

// import AddProjectProps from '../interfaces/add_project_props';
import FundWalletProps from '../interfaces/fund_wallet_props';
import { RootState } from '../redux/types';
import {
  depositMoney
} from '../redux/wallet/actions';

import Modal from 'react-bootstrap/Modal';
import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form';
import Alert from 'react-bootstrap/Alert';
import Spinner from 'react-bootstrap/Spinner';

let FundWalletModal: any = function(props: FundWalletProps) {
  // if (props.project === null) {
  //   return null;
  // }

  const dispatch = useDispatch();
  const [showFormError, setShowFormError] = useState(false);
  const [amount, setAmount] = useState<number>(0);
  const [isLoading, setIsLoading] = useState(false);
  const [showError, setShowError] = useState(false);
  const [requestError, setRequestError] = useState<String>('');
  const [showSuccess, setShowSuccess] = useState(false);
  
  return(
    <Modal show={props.show} onHide={() => props.onClose()} size="lg" scrollable centered>
      <Modal.Header closeButton>
        <h5 className="modal-title">Fund Wallet</h5>
      </Modal.Header>
      <Modal.Body className="pb-4 mx-md-5">
        {
          showFormError
          ? <Alert variant="danger" onClose={() => setShowFormError(false)} dismissible>
              <p>
                Enter an amount to send
              </p>
            </Alert>
          : null
        }
        {
          showError
          ? <Alert variant="danger" onClose={() => {}} dismissible>
              <Alert.Heading>Oh snap! You got an error!</Alert.Heading>
              <p>
                {requestError}
              </p>
            </Alert>
          : null
        }
        {
          showSuccess
            ? <Alert variant="success" onClose={() => setShowSuccess(false)} dismissible>
                <p>
                  ${amount} was deposited into your account successfully
                </p>
              </Alert>
            : null
        }
          <Form>
            <Form.Group controlId="formBasicEmail">
              <Form.Label>Amount</Form.Label>
              <Form.Control
                type="number" 
                placeholder="Enter amount to deposit"
                onChange={(value: any) => {
                  setAmount(parseInt(value["nativeEvent"]["target"]["value"]));
                }}
                required/>
            </Form.Group>
          </Form>
        
      </Modal.Body>
      <Modal.Footer>
        <Button variant="outline-secondary" onClick={() => {
          setIsLoading(false);
          setRequestError('');
          setShowError(false);
          setShowSuccess(false);
          props.onClose();
        }}>
          Close
        </Button>
        <Button 
          variant="primary" 
          disabled={isLoading}
          onClick={() => {
            if (amount < 1) {
              setShowFormError(true);
              return;
            }

            setIsLoading(true);
            dispatch(
              depositMoney(
                amount,
                () => {
                  setIsLoading(false);
                  setShowSuccess(true);
                },
                (errorMsg: String) => {
                  setIsLoading(false);
                  setRequestError(errorMsg);
                  setShowError(true);
                  setShowSuccess(false);
                }
              )
            );
          }
        }>
          Fund Wallet
          {
            isLoading
              ? <Spinner
                  as="span"
                  animation="border"
                  size="sm"
                  role="status"
                  aria-hidden="true"
                  className="ml-2"
                /> 
              : null
          }
        </Button>
      </Modal.Footer>
    </Modal>
  );
};

export default FundWalletModal;