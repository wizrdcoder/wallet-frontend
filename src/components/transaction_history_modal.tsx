import TransactionHistoryProps from '../interfaces/transaction_history_modal_props';

import Modal from 'react-bootstrap/Modal';
import Button from 'react-bootstrap/Button';

let TransactionHistoryModal: any = function(props: TransactionHistoryProps) {
  
  return(
    <Modal show={props.show} onHide={() => props.onClose()} size="lg" scrollable centered>
      <Modal.Header closeButton>
        <h5 className="modal-title">Transaction History</h5>
      </Modal.Header>
      <Modal.Body className="pb-4 mx-md-5">
        {
          props.transactions && props.transactions.length > 0
            ? <ul className="list-group mt-5 mx-md-5">
                {
                  props.transactions.map((item, i) => {
                    return <li className="list-group-item">
                      <div className="d-flex">
                        <h6 className="mr-auto">
                          { `$${item.amount}` }
                        </h6>
                        <h6 className="font-weight-light">
                          { item.created_at.split('.')[0].replace('-', '.').replace('T', ' - ').split(':')[0].split(' - ')[0] }
                        </h6>
                      </div>
                    </li>;
                  })
                }
              </ul>
            : null
        }
      </Modal.Body>
      <Modal.Footer>
        <Button variant="outline-secondary" onClick={() => props.onClose() }>
          Close
        </Button>
      </Modal.Footer>
    </Modal>
  );
};

export default TransactionHistoryModal;