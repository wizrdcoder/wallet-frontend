import axios from 'axios';
import { APIConstants } from '../constants/api';

/*
 * Configure axios to automatically add baseUrl and authorization to needed api request
 */

function getAxios() {
  let token = localStorage.getItem('token')
  if (token) {
    return axios.create({
      baseURL: APIConstants.BASE_ROUTE,
      headers: {
        Authorization: 'Bearer ' + token
      }
    })
  } else {
    return axios.create({
      baseURL: APIConstants.BASE_ROUTE
    })
  }
}

export default getAxios();

