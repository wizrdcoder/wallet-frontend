import { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Link, Redirect, useRouteMatch } from 'react-router-dom';
import {RootState} from '../../redux/types'

import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form';
import Spinner from 'react-bootstrap/Spinner';
import Alert from 'react-bootstrap/Alert';

import { loginRequest, clearLoginError } from '../../redux/auth/actions';
import { LocalstorageConstants } from '../../constants/localstorage';

export default function AdminLogin() {
  const match = useRouteMatch();
  const dispatch = useDispatch();
  // const authState = useSelector((state: RootState) => state.AdminAuthReducer)

  const [formData, setFormData] = useState<any>({
    email: '',
    password: ''
  });
  const [isLoggedIn, setIsLoggedIn] = useState(false);
  const [shouldShowError, setShouldShowError] = useState<boolean>(false);
  const [errorMessage, setErrorMessage] = useState<String>('');
  const [isLoading, setIsLoading] = useState<boolean>(false);

  useEffect(() => {
    const token = localStorage.getItem(LocalstorageConstants.AUTH_TOKEN_KEY);
    if (token != null) {
      setIsLoggedIn(true);
    } else {
      setIsLoggedIn(false);
    }
  }, []);
  
  function handleFormTextInput(label: any, value: String) {
    if (shouldShowError) {
      setShouldShowError(false);
    }

    let data: Map<String, String> = {
       ...formData,
       [label]: value 
    };

    setFormData(data);
  }
  
  return (
    <div className="row mt-5">
      {
        !isLoggedIn ? null : <Redirect to={`${match.path.replace('login', '')}`}/>
      }
      <div className="col-md-6 m-auto">
        <div className="card card-body">
          <h3 className="text-center mb-3"><i className="fas fa-sign-in-alt"></i> Login to Wallet</h3>
          {
            shouldShowError
              ? <div className="alert alert-danger mb-4" role="alert">
                  { errorMessage }
                </div>
              : null
          }
          <Form>
            <Form.Group controlId="formBasicEmail">
              <Form.Label>Email</Form.Label>
              <Form.Control
                type="email" 
                placeholder="Enter email"
                onChange={(value: any) => {
                  handleFormTextInput('email', value["nativeEvent"]["target"]["value"]);
                }}
                required/>
            </Form.Group>

            <Form.Group controlId="formBasicPassword">
              <Form.Label>Password</Form.Label>
              <Form.Control 
                type="password" 
                placeholder="Password" 
                onChange={(value: any) => {
                  handleFormTextInput('password', value["nativeEvent"]["target"]["value"]);
                }}
                required/>
            </Form.Group>
            <div className="row">
              <div className="col text-center">
                <Button
                  onClick={() => {
                    if (formData.email.length > 0 && formData.password.length > 0) {
                      setIsLoading(true);
                      
                      dispatch(
                        loginRequest(
                          formData,
                          () => {
                            setIsLoading(false);
                            window.location.pathname = '/'
                          },
                          (errorMsg: String) => {
                            setIsLoading(false);
                            setErrorMessage(errorMsg);
                            setShouldShowError(true);
                          }
                        )
                      );
                    } else {
                      setErrorMessage('All fields are required');
                      setShouldShowError(true);
                    }
                  }}
                  variant="primary"
                  disabled={isLoading}>
                    Submit
                    {
                      isLoading
                        ? <Spinner
                            as="span"
                            animation="border"
                            size="sm"
                            role="status"
                            aria-hidden="true"
                            className="ml-2"
                          /> 
                        : null
                    }
                </Button>
                <p className="mt-4">
                  Don't have an account? <Link to="/register">Signup</Link>
                </p>
              </div>
            </div>
          </Form>
        </div>
      </div>
    </div>
  );
}