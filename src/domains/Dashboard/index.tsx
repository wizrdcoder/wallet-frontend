import { useEffect, useState } from 'react';
import { useDispatch } from 'react-redux';
import {
  Switch,
  Route,
  Redirect,
  useRouteMatch
} from 'react-router-dom';

import { logoutAdmin } from '../../redux/auth/actions';

import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';
import Button from 'react-bootstrap/Button';

import {LocalstorageConstants} from '../../constants/localstorage';

import Dashboard from './dashboard';
import TransactionHistory from './transaction_history';

export default function DashboardContainer() {
  const match = useRouteMatch();
  const dispatch = useDispatch();
  const [isLoggedIn, setIsLoggedIn] = useState(true);
  const [showAddModal, setShowAddModal] = useState(false);
  const [searchTerm, setSearchTerm] = useState('');
  const [loggedInUser, setLoggedInUser] = useState<string>();

  useEffect(() => {
    const token = localStorage.getItem(LocalstorageConstants.AUTH_TOKEN_KEY);
    const user = localStorage.getItem(LocalstorageConstants.LOGGED_IN_USER_KEY);
    if (token != null) {
      setIsLoggedIn(true);
      setLoggedInUser(user ?? '');
    } else {
      setIsLoggedIn(false);
    }
  }, []);

  return (
    <div>
      {
        isLoggedIn ? null : <Redirect to={`/login`}/>
      }
      <Navbar bg="dark" variant="dark" expand="lg">
        <Navbar.Brand onClick={() => {}}>Wallet</Navbar.Brand>
        <Navbar.Toggle aria-controls="basic-navbar-nav" />
        <Navbar.Collapse id="basic-navbar-nav">
          <Nav className="ml-auto">
            {
              loggedInUser     
                ? <Nav.Link active>{ `Welcome ${loggedInUser}` }</Nav.Link>
                : null
            }
          </Nav>
          
          
          <Button 
            onClick={async () => {
              dispatch(logoutAdmin());
              window.location.href = 'login';
            }}
            variant="danger" 
            className="my-2 mx-4 my-sm-0 ml-auto"
          >
            Logout
          </Button>
        </Navbar.Collapse>
      </Navbar>

      {/* <AddProjectModal 
        show={showAddModal}
        onClose={() => setShowAddModal(false)}
        onSave={(project) => {
          dispatch(
            addProjectRequest(
              project, 
              () => {
                setShowAddModal(false);
              }
            )
          );
        }}
      /> */}

      <Switch>
        <Route path={`${match.path}history`}>
          <TransactionHistory />
        </Route>
        <Route path={match.path} >
          <Dashboard />
        </Route>
      </Switch>
    </div>
  );
}