import {useEffect, useState} from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {
  useRouteMatch
} from 'react-router-dom';

import { faShareSquare } from '@fortawesome/free-solid-svg-icons';
import { 
  getWalletBalance,
  getUsers,
  getHistory
} from '../../redux/wallet/actions';
import { RootState } from '../../redux/types';

import SendMoneyModal from '../../components/send_money_modal';
import FundWalletModal from '../../components/fund_wallet_modal';
import TransactionHistoryModal from '../../components/transaction_history_modal';

export default function Dashboard() {
  const match = useRouteMatch();
  const dispatch = useDispatch();
  const walletState = useSelector((state: RootState) => state.WalletReducer);
  const [showSendMoneyModal, setShowSendMoneyModal] = useState(false);
  const [showFundWalletModal, setShowFundWalletModal] = useState(false);
  const [showHistoryModal, setShowHistoryModal] = useState(false);

  useEffect(() => {
    dispatch(getWalletBalance());
    dispatch(getUsers());
    dispatch(getHistory());
  }, []);


  return (
    <div>
      <SendMoneyModal 
        show={showSendMoneyModal}
        onSend={() => {}}
        users={walletState.users}
        onClose={() => {
          setShowSendMoneyModal(false);
        }} 
      />

      <FundWalletModal 
        show={showFundWalletModal}
        onSend={() => {}}
        onClose={() => {
          setShowFundWalletModal(false);
        }} 
      />

      <TransactionHistoryModal 
        show={showHistoryModal}
        transactions={walletState.history}
        onClose={() => {
          setShowHistoryModal(false);
        }} 
      />

      <div className="container mt-5">
        <div className="row justify-content-center">
          <div className="col-md-8 m-auto text-center">
            {
              !walletState.balanceLoading
                ? <h1 className="mb-0">
                    ${walletState.balance}
                  </h1>
                : <h3 className="mb-0 font-weight-normal">
                    Getting balance...
                  </h3>
            }
            <span>Wallet balance</span>
            <div className="card mt-5 mx-md-5">
              <div className="card-body">
                <div className="row justify-content-center">
                  <div 
                    className="card"
                    style={{
                      cursor: 'pointer'
                    }}
                    onClick={() => setShowSendMoneyModal(true)}
                  >
                    <div className="card-body">
                      <FontAwesomeIcon 
                        icon={faShareSquare}
                        size="4x"
                        color="rgba(0, 0, 150, 0.4)" />
                      <h6 className="mt-4 font-weight-light">Send money</h6>
                    </div>
                  </div>
                </div>
                <button
                  type="button" 
                  className="btn btn-primary mt-4"
                  onClick={() => setShowFundWalletModal(true)}
                >
                    Fund Wallet
                </button>
              </div>
            </div>
            
            {
              walletState.history && walletState.history.length > 0
                ? <ul className="list-group mt-5 mx-md-5">
                    {
                      walletState.history.map((item, i) => {
                        if (i > 5) {
                          return null;
                        }

                        return <li className="list-group-item px-4">
                          <div className="d-flex">
                            <h6 className="mr-auto">
                              { `$${item.amount}` }
                            </h6>
                            <h6 className="font-weight-light">
                              { item.created_at.split('.')[0].replace('-', '.').replace('T', ' - ').split(':')[0].split(' - ')[0] }
                            </h6>
                          </div>
                        </li>;
                      })
                    }
                  </ul>
                : null
            }
            {
              walletState.history && walletState.history.length > 0
                ? <button
                    type="button" 
                    className="btn btn-outline-primary px-5 mt-4 mb-5"
                    onClick={() => setShowHistoryModal(true)}
                  >
                      See all
                    </button>
                : null
            }
          </div>
        </div>
      </div>
    </div>
  );
}