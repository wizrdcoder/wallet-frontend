import {useEffect, useState} from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { RootState } from '../../redux/types';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

export default function TransactionHistory() {
  const dispatch = useDispatch();
  const walletState = useSelector((state: RootState) => state.WalletReducer);
  
  return (
    <div>
      <div className="container mt-5">
        <div className="row justify-content-center">
          <div className="col-md-8 m-auto text-center">
            <h4>Transaction History</h4>
            {
              walletState.history && walletState.history.length > 0
                ? <ul className="list-group mt-5 mx-md-5">
                    {
                      walletState.history.map((item, i) => {
                        return <li className="list-group-item">
                          { item.amount }
                        </li>;
                      })
                    }
                  </ul>
                : null
            }
          </div>
        </div>
      </div>
    </div>
  );
}