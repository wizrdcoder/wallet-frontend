import { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Link, Redirect, useRouteMatch } from 'react-router-dom';
import {RootState} from '../../redux/types';

import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form';
import Spinner from 'react-bootstrap/Spinner';
import Alert from 'react-bootstrap/Alert';

import {
  registerRequest,
  clearLoginError, 
} from '../../redux/auth/actions';
import { LocalstorageConstants } from '../../constants/localstorage';

export default function AdminLogin() {
  const match = useRouteMatch();
  const dispatch = useDispatch();
  const [formDetails, setFormDetails] = useState<any>({
    first_name: '',
    last_name: '',
    email: '',
    password: ''
  });
  const [shouldShowError, setShouldShowError] = useState<boolean>(false);
  const [showSuccess, setShowSuccess] = useState<boolean>(false);
  const [errorMessage, setErrorMessage] = useState<String>('');
  const [isLoading, setIsLoading] = useState<boolean>(false);
  const [shouldRedirect, setShouldRedirect] = useState<boolean>(false);

  useEffect(() => {
  });

  function handleFormTextInput(label: any, value: String) {
    if (shouldShowError) {
      setShouldShowError(false);
    }

    let formData: Map<String, String> = {
       ...formDetails,
       [label]: value 
    };

    setFormDetails(formData);
  }
  
  return (
    <div className="row mt-5">
      {
        shouldRedirect 
          ? <Redirect to={`${match.path.replace('register', 'login')}`}/>
          : null
      }
      <div className="col-md-6 m-auto">
        <div className="card card-body">
          <h3 className="text-center mb-3"><i className="fas fa-sign-in-alt"></i> Register for Wallet</h3>
          {
            shouldShowError
              ? <div className="alert alert-danger mb-4" role="alert">
                  { errorMessage }
                </div>
              : null
          }
          {
            showSuccess
              ? <div className="alert alert-success mb-4" role="alert">
                  <h5 className="alert-heading">Registration successful!</h5>
                  <p>Your account registration was successful. Redirecting to Login</p>
                </div>
              : null
          }
          <Form>
            <Form.Group controlId="formBasicEmail">
              <Form.Label>First Name</Form.Label>
              <Form.Control
                type="text" 
                placeholder="Enter first name"
                onChange={(value: any) => {
                  handleFormTextInput('first_name', value["nativeEvent"]["target"]["value"]);
                }}
                required/>
            </Form.Group>

            <Form.Group controlId="formBasicEmail">
              <Form.Label>Last Name</Form.Label>
              <Form.Control
                type="text" 
                placeholder="Enter last name"
                onChange={(value: any) => {
                  handleFormTextInput('last_name', value["nativeEvent"]["target"]["value"]);
                }}
                required/>
            </Form.Group>

            <Form.Group controlId="formBasicEmail">
              <Form.Label>Email</Form.Label>
              <Form.Control
                type="email" 
                placeholder="Enter email"
                onChange={(value: any) => {
                  handleFormTextInput('email', value["nativeEvent"]["target"]["value"]);
                }}
                required/>
            </Form.Group>

            <Form.Group controlId="formBasicPassword">
              <Form.Label>Password</Form.Label>
              <Form.Control 
                type="password" 
                placeholder="Password" 
                onChange={(value: any) => {
                  handleFormTextInput('password', value["nativeEvent"]["target"]["value"]);
                }}
                required/>
            </Form.Group>
            <div className="row">
              <div className="col text-center">
                <Button
                  onClick={() => {
                    if (
                          formDetails.first_name.length > 0 
                          && formDetails.last_name.length > 0
                          && formDetails.email.length > 0
                          && formDetails.password.length > 0
                    ) {
                      setIsLoading(true);
                      
                      dispatch(
                        registerRequest(
                          formDetails, 
                          () => {
                            setIsLoading(false);
                            setShowSuccess(true);
                            setTimeout(() => {
                              setShouldRedirect(true);
                            }, 2000);
                          },
                          (errorMsg: String) => {
                            setIsLoading(false);
                            setErrorMessage(errorMsg);
                            setShouldShowError(true);
                          }
                        )
                      );
                    } else {
                      console.log('All fields are required!');
                      setErrorMessage('All fields are required');
                      setShouldShowError(true);
                    }
                  }}
                  variant="primary"
                  disabled={isLoading}>
                    Submit
                    {
                      isLoading
                        ? <Spinner
                            as="span"
                            animation="border"
                            size="sm"
                            role="status"
                            aria-hidden="true"
                            className="ml-2"
                          /> 
                        : null
                    }
                </Button>
                <p className="mt-4">
                  Already have an account? <Link to="/login">Login</Link>
                </p>
              </div>
            </div>
          </Form>
        </div>
      </div>
    </div>
  );
}