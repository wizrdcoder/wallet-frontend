import {
  Switch,
  Route,
  useRouteMatch
} from 'react-router-dom';

import Login from './Login';
import Register from './Register';
import Dashboard from './Dashboard';

export default function Home() {
  let match = useRouteMatch();
  
  return (
    <Switch>
      <Route path="/" >
        <Dashboard />
      </Route>
      <Route path={`${match.path}login`} >
        <Login />
      </Route>
      <Route path={`${match.path}register`}>
        <Register />
      </Route>
    </Switch>
  );
}