export default interface Transaction {
  id: number
  user_id: number
  amount: string
  from: string
  type: string
  remarks: string
  created_at: string
  updated_at: string
}