import User from '../interfaces/user';

export default interface SendMoneyProps {
  onClose: Function
  show: boolean
  users: User[]
}