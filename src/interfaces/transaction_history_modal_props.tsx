import Transaction from './transaction';

export default interface TransactionHistoryProps {
  onClose: Function
  transactions: Transaction[]
  show: boolean
}