import {combineReducers} from 'redux';
import WalletReducer from './wallet/reducer';

const reducers = combineReducers({
  WalletReducer
});

export default reducers;