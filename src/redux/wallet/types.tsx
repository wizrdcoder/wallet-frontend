import User from '../../interfaces/user';
import Transaction from '../../interfaces/transaction';

export const SET_WALLET_BALANCE = 'SET_WALLET_BALANCE';
export const SET_USERS = 'SET_USERS';
export const SET_HISTORY = 'SET_HISTORY';
export const SET_BALANCE_LOADING = 'SET_BALANCE_LOADING';


interface SetWalletBalance {
  type: typeof SET_WALLET_BALANCE,
  payload: number
}

interface SetBalanceLoading {
  type: typeof SET_BALANCE_LOADING,
  payload: boolean
}

interface SetUsers {
  type: typeof SET_USERS,
  payload: User[]
}

interface SetHistory {
  type: typeof SET_HISTORY,
  payload: Transaction[]
}

// interface GetProjects {
//   type: typeof SET_PROJECTS
//   payload: Project[]
// }

// interface GetProjectsLoading {
//   type: typeof GET_PROJECTS_LOADING,
//   payload: boolean
// }

// interface GetProjectsError {
//   type: typeof GET_PROJECTS_ERROR,
//   payload: string
// }

// interface addProject {
//   type: typeof ADD_PROJECT
// }

// interface addProjectLoading {
//   type: typeof ADD_PROJECT_LOADING,
//   payload: boolean
// }

// interface addProjectError {
//   type: typeof ADD_PROJECT_ERROR,
//   payload: string
// }

// interface clearError {
//   type: typeof CLEAR_ERROR
// }

// interface deleteProject {
//   type: typeof DELETE_PROJECT
//   payload: string
// }

// interface editProject {
//   type: typeof EDIT_PROJECT
// }

// interface searchProjects {
//   type: typeof SEARCH_PROJECTS
//   payload: { projects: Project[], searchTerm: string }
// }

// interface clearSearch {
//   type: typeof CLEAR_SEARCH
// }

// interface sortProjects {
//   type: typeof SORT_PROJECTS,
//   payload: Sorts
// }

// interface filterProjects {
//   type: typeof FILTER_PROJECTS,
//   payload: Filter
// }

// interface resetFilter {
//   type: typeof RESET_FILTER
// }

// interface changeProjectStatus {
//   type: typeof CHANGE_PROJECT_STATUS,
//   payload: {index: number, status: number, time: string}
// }

export interface WalletState {
  // projects: Project[]
  // loading: boolean
  // errorMessage: string
  // addProjectError: string
  // isSearchMode: boolean,
  // searchTerm: string
  // activeSorting: Sorts
  // filter: Filter
  // isFilterApplied: boolean
  // filteredProjects: Project[]
  balance: number,
  balanceLoading: boolean,
  users: User[],
  history: Transaction[]
}

export type WalletActionTypes = SetWalletBalance
  | SetBalanceLoading
  | SetUsers
  | SetHistory
  // | GetProjectsLoading
  // | GetProjectsError
  // | addProject
  // | addProjectLoading
  // | addProjectError
  // | clearError
  // | deleteProject
  // | editProject
  // | searchProjects
  // | clearSearch
  // | sortProjects
  // | filterProjects
  // | resetFilter
  // | changeProjectStatus