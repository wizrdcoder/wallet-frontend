// import axios from 'axios';
import { APIConstants } from '../../constants/api';
import { LocalstorageConstants } from '../../constants/localstorage';
import NewUser from '../../models/user';
import NewTransaction from '../../models/transactiont';

import axios from '../../utils/config';

import {
  SET_WALLET_BALANCE,
  SET_USERS,
  SET_BALANCE_LOADING,
  SET_HISTORY
} from './types';
import { awaitExpression } from '@babel/types';
import User from '../../interfaces/user';
import Transaction from '../../interfaces/transaction';

export function getWalletBalance() {
  return async function(dispatch: any) {
    await dispatch(setBalanceLoading(true));
    
    try {
      const res = await axios.get(APIConstants.WALLET_BALANCE_ROUTE);

      if (res.status == 200 || res.status == 201) {
        await dispatch(setWalletBalance(res.data.data[0].balance));
      }
    } catch(e) {
      await dispatch(setBalanceLoading(false));
    }
  }
}

export function getUsers() {
  return async function(dispatch: any) {
    try {
      const res = await axios.get(APIConstants.ALL_USERS_ROUTE);

      if (res.status == 200 || res.status == 201) {
        // console.log('all users route', res.data);
      
        let users = res.data.data.map((user: any, i: number) => {
          let newUser = new NewUser();
          newUser.first_name = user.first_name;
          newUser.last_name = user.last_name;
          newUser.email = user.email;
          newUser.id = user.id;
          return newUser;
        });

        await dispatch(setUsers(users));
      }
    } catch(e) {

    }
  }
}

export function depositMoney(amount: number, onFinished: Function, onError: Function) {
  return async function(dispatch: any) {
    try {
      const res = await axios.post(
        APIConstants.ADD_MONEY_ROUTE,
        {
          'amount': amount
        }
      );

      if (res.status == 200 || res.status == 201) {
        await dispatch(getWalletBalance());
        onFinished();
        return;
      }

      onError('An error occurred processing request. Please try again later');
    } catch(e) {
      onError('An error occurred processing request. Please try again later');
    }
  }
}

export function getHistory() {
  return async function(dispatch: any) {
    try {
      const res = await axios.get(APIConstants.HISTORY_ROUTE);

      if (res.status == 200 || res.status == 201) {
        console.log('history route', res.data);
        let history = res.data.data.map((item: any, i: number) => {
          let newTransaction = new NewTransaction();
          newTransaction.amount = item.amount;
          newTransaction.created_at = item.created_at;
          newTransaction.from = item.from;
          newTransaction.user_id = item.user_id;
          return newTransaction;
        });

        await dispatch(setHistory(history));
      }
    } catch(e) {

    }
  }
}

export function sendMoney(amount: number, id: number, onFinished: Function, onError: Function) {
  return async function(dispatch: any) {
    try {
      const res = await axios.post(
        APIConstants.SEND_MONEY_ROUTE,
        {
          'amount': amount,
          'reciever_id': id
        }
      );

      if (res.status == 200 || res.status == 201) {
        console.log('transfer result', res.data);
        await dispatch(getWalletBalance());
        onFinished();
        return;
      }
      console.log('transfer result', res.data);

      // onError('An error occurred processing request. Please try again later');
    } catch(e) {

      onError('Insufficient balance. Fund your wallet and try again. ');
    }
  }
}

export function setWalletBalance(balance: number) {
  return {
    type: SET_WALLET_BALANCE,
    payload: balance
  }
}

export function setUsers(users: User[]) {
  return {
    type: SET_USERS,
    payload: users
  }
}

export function setBalanceLoading(isLoading: boolean) {
  return {
    type: SET_BALANCE_LOADING,
    payload: isLoading
  }
}

export function setHistory(history: Transaction[]) {
  return {
    type: SET_HISTORY,
    payload: history
  }
}