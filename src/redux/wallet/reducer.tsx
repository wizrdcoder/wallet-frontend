import {
  SET_WALLET_BALANCE,
  SET_USERS,
  SET_HISTORY,
  WalletActionTypes,
  WalletState,
  SET_BALANCE_LOADING
} from './types';

const initialState: WalletState = {
  balance: 0,
  balanceLoading: true,
  users: [],
  history: []
}

export default function projectsReducer(
  state = initialState,
  action: WalletActionTypes
): WalletState {
  switch (action.type) {
    case SET_WALLET_BALANCE:
      return {
        ...state,
        balance: action.payload,
        balanceLoading: false
      }
    case SET_USERS:
      return {
        ...state,
        users: action.payload
      }
    case SET_BALANCE_LOADING:
      return {
        ...state,
        balanceLoading: action.payload
      }
    case SET_HISTORY:
      return {
        ...state,
        history: action.payload
      }
    default:
      return state;
  }
}