import { createStore, applyMiddleware, compose } from 'redux';
import ReduxThunk from 'redux-thunk';
import reducers from './reducers';

const middlewares = [ReduxThunk];

export default function configReduxStore() {
  const store = createStore(
    reducers,
    applyMiddleware(...middlewares)
  );

  return store;
}