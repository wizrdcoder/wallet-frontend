import {WalletState} from './wallet/types';

export interface RootState {
  WalletReducer: WalletState
}