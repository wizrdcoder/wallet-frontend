import {
  LOGIN_ADMIN,
  LOGOUT_ADMIN,
  SET_ADMIN_LOGIN_ERROR,
  AdminAuthActionTypes,
  ADMIN_AUTH_LOADING,
  CLEAR_LOGIN_ERROR,
  AdminAuthState
} from './types';

const initialState: AdminAuthState = {
  username: '',
  password: '',
  loading: false,
  errorMessage: '',
  isLoggedIn: false
}

export default function adminAuthReducer(
  state = initialState,
  action: AdminAuthActionTypes
): AdminAuthState {
  switch (action.type) {
    case ADMIN_AUTH_LOADING:
      return {
        ...state,
        loading: true,
        errorMessage: ''
      }
    case LOGIN_ADMIN:
      return {
        ...state,
        loading: false,
        errorMessage: '',
        isLoggedIn: true
      };
    case SET_ADMIN_LOGIN_ERROR:
      return {
        ...state,
        errorMessage: action.payload,
        loading: false
      }
    case CLEAR_LOGIN_ERROR:
      return {
        ...state,
        errorMessage: ''
      }
    case LOGOUT_ADMIN:
      return {
        ...state,
        isLoggedIn: false
      }
    default:
      return state;
  }
}