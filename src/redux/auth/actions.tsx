import axios from 'axios';

import { APIConstants } from '../../constants/api';
import { LocalstorageConstants } from '../../constants/localstorage';
import {
  LOGIN_ADMIN,
  LOGOUT_ADMIN,
  SET_ADMIN_LOGIN_ERROR,
  ADMIN_AUTH_LOADING,
  CLEAR_LOGIN_ERROR,
  AdminAuthActionTypes
} from './types';

export function registerRequest(formData: any, onFinished: Function, onError: Function) {
  return async function(dispatch: any) {
    try {
      // const formdata = new FormData();
      // formdata.append('username', username);
      // formdata.append('password', password);

      console.log({...formData});
      
      let res = await axios.post(
        `${APIConstants.BASE_ROUTE}${APIConstants.SIGNUP_ROUTE}`,
        {
          ...formData,
          password_confirmation: formData['password']
        }
      );

      console.log('signup result', res.status);
      console.log('signup result', res.data);

      if (res.status == 201 || res.status == 200) {
        // save user details to localStorage
        localStorage.setItem(LocalstorageConstants.USER_DATA_KEY, JSON.stringify({
          first_name: formData['first_name'],
          last_name: formData['last_name'],
          email: formData['email']
        }));

        onFinished();
      } else {
        onError('An error occurred while processing request. Please try again later');
      }
      
      // localStorage.setItem(LocalstorageConstants.ADMIN_USERNAME_KEY, username);
      // localStorage.setItem(LocalstorageConstants.ADMIN_PASSWORD_KEY, password);
      // return dispatch(loginAdmin(username, password));
    } catch (e: any) {
      if (e.message.indexOf('400') >= 0 || e.message.indexOf('500') >= 0) {
        onError('An error occurred while processing request. Please try again later');
        return;
      }
      if (e.message.indexOf('401') >= 0) {
        onError('An error occurred while processing request. Please try again later');
        return;
      }

      onError('An error occurred while processing request. Please try again later');
    }
  };
}

export function loginRequest(formData: any, onFinished: Function, onError: Function) {
  return async function(dispatch: any) {
    await dispatch(setLoading(false));

    try {
      let res = await axios.post(
        `${APIConstants.BASE_ROUTE}${APIConstants.LOGIN_ROUTE}`,
        { ...formData }
      );

      if (res.status == 201 || res.status == 200) {
        // save token to localStorage
        localStorage.setItem(LocalstorageConstants.AUTH_TOKEN_KEY, res.data.access_token);
        localStorage.setItem(
          LocalstorageConstants.LOGGED_IN_USER_KEY,
          `${res.data.first_name} ${res.data.first_name}` 
        );

        onFinished();
      } else {
        onError('An error occurred while processing request. Please try again later');
      }
    } catch (e: any) {
      if (e.message.indexOf('400') >= 0 || e.message.indexOf('500') >= 0) {
        onError('An error occurred while processing request. Please try again later');
        return;
      }
      if (e.message.indexOf('401') >= 0) {
        onError('Incorrect username or password');
        return;
      }

      onError('An error occurred while processing request. Please try again later');
    }
  }
}

export function loginAdmin(username: string, password: string): AdminAuthActionTypes {
  return {
    type: LOGIN_ADMIN,
    payload: {
      'username': username,
      'password': password
    }
  };
}

export function setLoginError(message: string) {
  return {
    type: SET_ADMIN_LOGIN_ERROR,
    payload: message
  }
}

export function setLoading(loading: boolean) {
  return {
    type: ADMIN_AUTH_LOADING,
    payload: loading
  }
}

export function clearLoginError() {
  return {
    type: CLEAR_LOGIN_ERROR
  }
}

export function logoutAdmin() {
  return function(dispatch: any) {
    localStorage.removeItem(LocalstorageConstants.ADMIN_USERNAME_KEY);
    localStorage.removeItem(LocalstorageConstants.ADMIN_PASSWORD_KEY);
    localStorage.removeItem(LocalstorageConstants.USER_DATA_KEY);
    localStorage.removeItem(LocalstorageConstants.AUTH_TOKEN_KEY);

    // window.location.href = window.location.href.split(window.location.pathname)[0]
    dispatch(logout());
  }
}

export function logout() {
  return {
    type: LOGOUT_ADMIN
  }
}