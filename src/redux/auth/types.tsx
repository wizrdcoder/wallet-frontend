export const LOGIN_ADMIN = 'LOGIN_ADMIN';
export const LOGOUT_ADMIN = 'LOGOUT_ADMIN';
export const SET_ADMIN_LOGIN_ERROR = 'SET_ADMIN_LOGIN_ERROR';
export const SET_ADMIN_LOGIN_INFO = 'SET_ADMIN_LOGIN_INFO';
export const ADMIN_AUTH_LOADING = 'ADMIN_AUTH_LOADING';
export const CLEAR_LOGIN_ERROR = 'CLEAR_LOGIN_ERROR';

interface LoginAdminAction {
  type: typeof LOGIN_ADMIN
  payload: {username: string, password: string}
}

interface LogoutAdminAction {
  type: typeof LOGOUT_ADMIN,
}

interface SetAdminLoginErrorAction {
  type: typeof SET_ADMIN_LOGIN_ERROR
  payload: string
}

interface SetAdminLoginInfoAction {
  type: typeof SET_ADMIN_LOGIN_INFO
  payload: {username: string, password: string}
}

interface SetAdminAuthLoading {
  type: typeof ADMIN_AUTH_LOADING
  payload: boolean
}

interface ClearLoginErrorAction {
  type: typeof CLEAR_LOGIN_ERROR
}

export interface AdminAuthState {
  username: string
  password: string
  loading: boolean
  errorMessage: string
  isLoggedIn: boolean
}


export type AdminAuthActionTypes = LoginAdminAction 
  | LogoutAdminAction 
  | SetAdminLoginErrorAction 
  | SetAdminLoginInfoAction
  | SetAdminAuthLoading
  | ClearLoginErrorAction